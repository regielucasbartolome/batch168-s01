<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Echoing Values</h1>


	<p><?php echo 'Hello Batch 168!'; ?></p>
	<p><?php echo 'Good day $name! Your given email is $email.'; ?> </p>
	<!-- Variables cannot be called using single quotes -->
	<p><?php echo "Good day $name! Your given email is $email."; ?> </p>
	<p><?php echo PI; ?></p>

	<!-- Data Types -->
	<h1>Data Types</h1>

	<p><?php echo "State is $state in $country."; ?></p>
	<p><?php echo "$address"; ?></p>
	<p><?php echo "$addressTwo"; ?></p>

	<!-- Normal Echoing of Boolean and Null Variables will not appear in the web output -->

	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>

	<!-- gettype function returns the type of a variable (like typeof in JS) -->
	<p><?php echo gettype($hasTravelledAbroad); ?></p>
	<!-- var_dump gives detail about the variable -->
	<p><?php echo var_dump($spouse); ?></p>

	<!-- Echoing Object -->
	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<!-- Echoing Array -->
	<p><?php echo $grades[3]; ?></p>
	<p><?php echo $grades[0]; ?></p>

	<!-- Operators -->

	<h1>Operators</h1>
	<p> x: <?php echo $x; ?></p>
	<p> y: <?php echo $y; ?></p>

	<p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
	<p>Is Registered: <?php echo var_dump($isRegistered); ?></p>

	<h2>Arithmetic Operations</h2>

	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>

	<h2>Equality Operators</h2>

	<p>Loose Equality: <?php echo var_dump($x == '56.2'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '56.2'); ?></p>
	<p>Loose Inequality: <?php echo var_dump($x != '56.2'); ?></p>
	<p>Strict Inequality: <?php echo var_dump($x !== '56.2'); ?></p>

	<h2>Greater/Lesser Than Operators</h2>
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>

	<h2>Logical Operators</h2>
	<p>Are all requirements Met: <?php echo var_dump($isLegalAge AND $isRegistered); ?></p>
	<p>Are some requirements Met: <?php echo var_dump($isLegalAge OR $isRegistered); ?></p>
	<p>Are some requirements not Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

	<h1>Functions</h1>
	<p>Full Name: <?php echo getFullName('John', 'D', 'Smith') ?></p>

	<h1>Selection Control Structures</h2>
	
	<h2>If-Elseif-Else</h2>

	<p>Wind Speed is: <?php echo determineTyphoonIntensity(30); ?></p>

	<h2>Ternary Operator</h2>

	<p>78: <?php echo var_dump(isUnderAge(78)); ?></p>
	<p>16: <?php echo var_dump(isUnderAge(16)); ?></p>

	<h2>Switch Statement</h2>

	<p><?php echo determineComputerUser(6); ?></p>

	<h2>Try-Catch-Finally</h2>

	<p><?php echo greeting('regie'); ?></p>

</body>
</html>