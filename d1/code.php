<?php

//=====PHP Comments=====
// this is a single line comment

/*
	this is a multiline comment
*/
//=====End PHP Comments=====

//=====Variables=====
// Variables - used to store values or contain data
// Variabes in PHP are defined using the dollar ($) notation before the name of the variable.

	$name = 'John Smith';
	$email = 'johnsmith@gmail.com';
//=====End of Variables=====

//=====Constants=====
// Constants are defined using the define('constant_name', constant_value) function

	define('PI', 3.1416);
//=====End of Constants

//=====Data Types=====

// Strings

	$state = 'New York';
	$country = 'United States of America';
	$address = $state.', '.$country; //concatination via dot notation
	$addressTwo = "$state, $country"; //concatination via double quotes

//Integers
	//handles whole numeric values
	$age = 31;
	$headcount = 26;

//Floats
	//handles numeric values with decimal
	$grade = 98.2;
	$distanceInKm = 2562.23;

//Boolean
	//handles boolean value (true or false)
	$hasTravelledAbroad = false;
	$haveSymptoms = true;

//Null
	//to delare certain variable where initially there is no value at all
	$spouse = null;
	$middleName = null;

//Array
	//can handle multiple value of the same type
	$grades = array(98.7, 92.1, 90.2, 94.6); //element-every value in the array; index-place of each element in the array

//Objects
	//properties: values
	$gradesObj = (object)[
		'firstGrading' => 98.7,
		'secondGrading' => 92.1,
		'thirdGrading' => 94.6,
		'fourthGrading' => 90.2
	];

	$personObj = (object)[
		'fullName' => 'John Smith',
		'isMarried' => false,
		'age' => 35,
		'address' => (object)[
			'state' => 'Washington DC',
			'country' => 'United States of America'
		]
	];
//=====End of Data Types=====

//=====Operators=====

	//Assignment Operators
	$x = 56.2;
	$y = 912.6;

	$isLegalAge = true;
	$isRegistered = false;

	//Arithmetic Operators
	//Greater/Lesser Than Operators
	//Logical Operators

//Function

	function getFullName($firstName, $middleInitial, $lastName) {
		return "$lastName, $firstName $middleInitial.";
	}

//Selection Control Structures

	//if-elseif-else statement

	function determineTyphoonIntensity($windSpeed){
		if($windSpeed < 30){
			return 'Not a typhoon yet';
		} elseif($windSpeed <= 61) {
			return 'Tropical depression detected.';
		} elseif($windSpeed >=62 && $windSpeed <= 88){
			return 'Tropical storm detected';
		} elseif($windSpeed >=89 && $windSpeed <= 177){
			return 'Severe tropical storm detected';
		} else {
			return 'Typhoon detected.';
		}
	}

//Conditional (Ternary) Operator

	function isUnderAge($age) {
		return ($age < 18) ? true : false;
	}

//Switch Statement
function determineComputerUser($computerNumber) {
	switch($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Albert Einstein';
			break;
		case 5:
			return 'Charles Babbage';
			break;
		default:
			return $computerNumber.' is out of bounds.';
	}
}

//=====Try-Catch-Finally=====

function greeting($str) {
	try{
		if(gettype($str) == "string"){
			echo $str;
		} else{
			throw new Exception("Oops!");
		}
	}
	catch(Exception $e) {
		echo $e->getMessage();
	}
	finally {
		echo " I did it again";
	}
}