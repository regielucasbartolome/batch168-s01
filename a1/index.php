<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S01-Activity 1</title>
</head>
<body>
	<h1>Full Address</h1>

	<p><?php echo getFullAddress('Philippines', 'Quezon City', 'NCR Second District', 'XYZ Building') ?></p>

	<h1>Letter-Based Grading</h1>

	<p><?php echo getLetterGrade(0); ?></p>
	<p><?php echo getAdjectivalRating(100); ?></p>

</body>
</html>