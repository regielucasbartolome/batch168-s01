<?php

function getFullAddress($Country, $City, $Province, $specificAdd) {
	return "$specificAdd, $City, $Province, $Country";
}
function getLetterGrade($grade) {
	switch ($grade) {
		case $grade>=98 && $grade<=100:
			return "$grade is equivalent to A+";
			break;

		case $grade>=95 && $grade<=97:
			return "$grade is equivalent to A";
			break;

		case $grade>=92 && $grade<=94:
			return "$grade is equivalent to A-";
			break;

		case $grade>=89 && $grade<=91:
			return "$grade is equivalent to B+";
			break;

		case $grade>=86 && $grade<=88:
			return "$grade is equivalent to B";
			break;

		case $grade>=83 && $grade<=85:
			return "$grade is equivalent to B-";
			break;

		case $grade>=80 && $grade<=82:
			return "$grade is equivalent to C+";
			break;

		case $grade>=77 && $grade<=79:
			return "$grade is equivalent to C";
			break;

		case $grade>=75 && $grade<=76:
			return "$grade is equivalent to C-";
			break;

		case $grade>=1 && $grade<=74:
			return "$grade is equivalent to D";
			break;

		case $grade==0:
			return "Please enter a valid grade from 1 - 100.";
			break;
		
		default:
			return "$grade is Out of range!";
			break;
	}
}

function getAdjectivalRating($grade){
	if (gettype($grade) !== 'string') {
		if($grade>=98 && $grade<=100){
			return "$grade is equivalent to A+";
		}else {
			return "Input is out of range.";
		}
	}else {
		return "Enter a valid grade ranging from 1 - 100.";
	}

}